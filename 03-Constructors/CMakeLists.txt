set(target ctor)

PYTHON_ADD_MODULE(${target} ${target}.cpp)
set_target_CXX_compile_options(${target})

FILE(COPY ${target}.py DESTINATION .)
get_filename_component(name ${CMAKE_CURRENT_SOURCE_DIR} NAME)
ADD_TEST(NAME ${name} COMMAND ${PYTHON_EXECUTABLE} ${target}.py)

# PYTHON_ADD_MODULE(ctor ctor.cpp)
# FILE(COPY ctor.py DESTINATION .)
# ADD_TEST(NAME 03-Constructors COMMAND ${PYTHON_EXECUTABLE} ctor.py)
