cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)
#-------------------------------------------------------------------------------
# Pick up the compilers from the environment
# . pick up the intel compilers if they are available, if not use GCC. If we are
#   running on a remote cluster (login or cmompute node) assert that we did not
#   pick up the system's GCC.
# . set some common compiler flags
#
# From https://cmake.org/pipermail/cmake/2011-May/044445.html
# method 1:
#
# CC=icc CXX=icpc FC=ifort cmake /path/to/source
#
# this will raise an error if the icc, icpc or ifort are not available
#
# We also do:
#
# include(CMakeForceCompiler)
# CMAKE_FORCE_C_COMPILER      (icc   "Intel C Compiler")
# CMAKE_FORCE_CXX_COMPILER    (icpc  "Intel C++ Compiler")
# CMAKE_FORCE_Fortran_COMPILER(ifort "Intel Fortran Compiler")
#
# Lastly, when using the Intel compiler, we add:
#
# if(${CMAKE_Fortran_COMPILER_ID} STREQUAL "Intel")
#    if(NOT CMAKE_Fortran_FLAGS_RELEASE)
#       set(CMAKE_Fortran_FLAGS_RELEASE "-O2 -xhost" CACHE STRING "" FORCE)
#    endif()
#   set(CMAKE_Fortran_FLAGS_DEBUG
#       "${CMAKE_Fortran_FLAGS_DEBUG} -check noarg_temp_created -C -traceback" CA\
# CHE STRING "" FORCE)
#   set(CMAKE_Fortran_FLAGS_DEBUGHEAVY
#       "${CMAKE_Fortran_FLAGS_DEBUG} -check noarg_temp_created -fpe0 -warn align\
# ments -warn declarations -warn general -warn interfaces -warn truncated_source \
# -warn uncalled -warn uninitialized -warn usage -common_args -warn unused -fp-st\
# ack-check -check bounds -check uninit -check format" CACHE STRING "" FORCE)
#    mark_as_advanced(CMAKE_Fortran_FLAGS_DEBUGHEAVY)
# endif()


#-------------------------------------------------------------------------------
function(IntelToolChain)
    message("<< IntelToolChain.cmake")
    if(VERBOSE)
        message("#  Compilers set:")
        message("#    C++    : ${CMAKE_CXX_COMPILER}")
        message("#    C      : ${CMAKE_C_COMPILER}")
        message("#    Fortran: ${CMAKE_Fortran_COMPILER}")
    endif()

    set(_vsc_home $ENV{VSC_HOME})
    if("${_vsc_home}" STREQUAL "")
        if(VERBOSE)
            message("#  running on: LOCAL")
        endif()
        if("${CMAKE_Fortran_COMPILER}" STREQUAL "")
            # Fortran compiler missing...
            message( WARNING
              " #  gfortran not found. Try \n"
              " #    > export FC=`which gfortran`\n"
              " #  before running cmake."
          )
        endif()
    else()
        if(VERBOSE)
            message("#  running on: $ENV{VSC_INSTITUTE_CLUSTER}")
        endif()
        if("${CMAKE_CXX_COMPILER}" STREQUAL "/usr/bin/g++")
            message("ERROR: Using system compiler for HPC is a bad idea.")
            message("       You must select appropriate compilers with")
            message("         > module load <compiler_suite>")
            message("       and export CXX/CC/FC.")
            message(FATAL_ERROR "")
        endif()
    endif()
    if(CMAKE_CXX_COMPILER)
        message("CMAKE_CXX_COMPILER_ID=${CMAKE_CXX_COMPILER_ID}")
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -Wall -DDEBUG")
        message("CMAKE_CXX_FLAGS_DEBUG   is ${CMAKE_CXX_FLAGS_DEBUG}")
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")
        message("CMAKE_CXX_FLAGS_RELEASE is ${CMAKE_CXX_FLAGS_RELEASE}")
        message("CMAKE_CXX_FLAGS         is ${CMAKE_CXX_FLAGS}")
    endif()
    message(">> IntelToolChain.cmake\n")
endfunction()
