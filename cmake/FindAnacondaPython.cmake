# This was modified after https://github.com/jkhoogland/FindPythonAnaconda
#
# tested on macOS Mojave and Ubuntu 18.10
# Find active Anaconda Python distribution
# FATAL_ERROR if conda not on the PATH, or if no Anaconda environment has been
# activated.
# If successful, sets
# . PYTHON_INCLUDE_DIRS
# . PYTHON_LIBRARIES
# . ANACONDA_ROOT			e.g. /Users/etijskens/miniconda3
# . ANACONDA_PYTHON_FOUND        [bool]
# . ANACONDA_ENV_ROOT		e.g. /Users/etijskens/miniconda3/envs/py-boost
# . PYTHON_VERSION_MAJOR	e.g. 3
# . PYTHON_VERSION_MINOR	e.g. 6
# . PYTHON_VERSION_PATCH	e.g. 4
#-------------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)
#-------------------------------------------------------------------------------
# Using a macro instead of a function, because it allows to return to the caller
# of FindAnacondaPython and set variables in the PARENT_SCOPE.
macro( msg _required_ _msg_ )
	if(${_required_})
		message( FATAL_ERROR "${_msg_}")
	else()
		message( WARNING "${_msg_}")
		set(ANACONDA_PYTHON_FOUND False PARENT_SCOPE)
		return()
	endif()
endmacro()

#-------------------------------------------------------------------------------
# Find Anaconda installation
Function(FindAnacondaPython)
	cmake_parse_arguments(
	    FAP "REQUIRED" "" "" ${ARGN}
	)
	message("<< FindPythonAnaconda.cmake")

  # for this command to work, conda must be on the PATH.
	execute_process(
	  COMMAND which conda
	  RESULT_VARIABLE _ret
	  OUTPUT_VARIABLE CONDA_EXE
	  ERROR_VARIABLE  _e
	  OUTPUT_STRIP_TRAILING_WHITESPACE
	  ERROR_STRIP_TRAILING_WHITESPACE
	)
	if(_ret)
		msg( "#  Conda not found.")
	else()
		if(VERBOSE)
			message("#  Conda found: ${CONDA_EXE}")
		endif()
	endif()
  # OK, conda is on the PATH
  	execute_process(
   		COMMAND conda info --root
	    RESULT_VARIABLE _ret
		OUTPUT_VARIABLE ANACONDA_ROOT
		ERROR_VARIABLE  _err
		OUTPUT_STRIP_TRAILING_WHITESPACE
		ERROR_STRIP_TRAILING_WHITESPACE
	)
	output("conda info --root"
	   	   "${_ret}" "${ANACONDA_ROOT}" "${_err}"
		  )

    if( IS_DIRECTORY ${ANACONDA_ROOT} )
		set(ANACONDA_ENV_ROOT "$ENV{CONDA_PREFIX}")
		set(ANACONDA_ENV_ROOT "${ANACONDA_ENV_ROOT}" PARENT_SCOPE)
		if(ANACONDA_ENV_ROOT)
			set(ANACONDA_PYTHON_FOUND True )
			set(ANACONDA_PYTHON_FOUND ${ANACONDA_PYTHON_FOUND} PARENT_SCOPE)
		else()
			msg( ${FAP_REQUIRED} "No activated Anaconda environment." )
		endif()
	endif()

	# message( "ANACONDA_ROOT         '${ANACONDA_ROOT}'" )
	# message( "CONDA_DEFAULT_ENV     '$ENV{CONDA_DEFAULT_ENV}'" )
	# message( "ANACONDA_PYTHON_FOUND '${ANACONDA_PYTHON_FOUND}'" )
	if( ANACONDA_PYTHON_FOUND )
		if(VERBOSE)
		message( "#  ANACONDA_ROOT     '${ANACONDA_ROOT}'" )
		message( "#  CONDA_DEFAULT_ENV '$ENV{CONDA_DEFAULT_ENV}'" )
		message( "#  ANACONDA_ENV_ROOT '${ANACONDA_ENV_ROOT}'" )
		endif()

		execute_process(
	      COMMAND python -c "import platform; print(platform.python_version())"
	      RESULT_VARIABLE _ret
	      OUTPUT_VARIABLE PYTHON_VERSION
	      ERROR_VARIABLE  _err
	      OUTPUT_STRIP_TRAILING_WHITESPACE
	      ERROR_STRIP_TRAILING_WHITESPACE
	    )
		output("python -c import platform; print(platform.python_version())"
				"${_ret}" "${_out}" "${_err}"
			  )

	    string (REGEX MATCH "([0-9]+)[.]([0-9]+)[.]([0-9]+)" _out "${PYTHON_VERSION}")
		set( PYTHON_VERSION_MAJOR ${CMAKE_MATCH_1} )
		set( PYTHON_VERSION_MAJOR ${CMAKE_MATCH_1} CACHE INTERNAL "" FORCE)
		set( PYTHON_VERSION_MINOR ${CMAKE_MATCH_2} )
	    set( PYTHON_VERSION_MINOR ${CMAKE_MATCH_2} CACHE INTERNAL "" FORCE)
		set( PYTHON_VERSION_PATCH ${CMAKE_MATCH_3} )
	    set( PYTHON_VERSION_PATCH ${CMAKE_MATCH_3} CACHE INTERNAL "" FORCE)
	    set( ANACONDA_PYTHON_VERSION ${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR} )
		if( ${PYTHON_VERSION_MAJOR} MATCHES 2 )
	        set( _py_ext "")
	    else()
	      	set( _py_ext "m")
	    endif()
	    set(PYTHON_ID "python${ANACONDA_PYTHON_VERSION}${_py_ext}")

		set(PYTHON_INCLUDE_DIRS "${ANACONDA_ENV_ROOT}/include/${PYTHON_ID}" CACHE INTERNAL "" FORCE)
		set(PYTHON_LIBRARIES    "${ANACONDA_ENV_ROOT}/lib/lib${PYTHON_ID}${CMAKE_SHARED_LIBRARY_SUFFIX}" CACHE INTERNAL "" FORCE)
		message(STATUS "Anaconda Python found:")
		multiline("${PYTHON_INCLUDE_DIRS}")
		message(STATUS "  PYTHON_INCLUDE_DIRS = ${out}")
		multiline("${PYTHON_LIBRARIES}")
		message(STATUS "  PYTHON_LIBRARIES = ${out}")
	endif()

	message(">> FindPythonAnaconda.cmake\n")
endfunction(FindAnacondaPython)
