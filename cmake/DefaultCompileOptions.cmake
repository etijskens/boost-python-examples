cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)
#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
function(set_target_CXX_compile_options myTarget)
    if(VERBOSE)
        message("<< set_target_CXX_compile_options(${myTarget}) [${CMAKE_CURRENT_SOURCE_DIR}]")
    endif()
    target_compile_features(${myTarget} PUBLIC cxx_std_11)
    set_target_properties(${myTarget} PROPERTIES CXX_EXTENSIONS OFF)
    if(VERBOSE)
        message(">> set_target_CXX_compile_options(${myTarget})")
    endif()
endfunction()
#-------------------------------------------------------------------------------
function(set_target_C_compile_options myTarget)
endfunction()
#-------------------------------------------------------------------------------
function(set_target_Fortran_compile_options myTarget)
endfunction()
#-------------------------------------------------------------------------------
