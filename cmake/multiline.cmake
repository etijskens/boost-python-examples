#-------------------------------------------------------------------------------
# Format a string of space-separated items as one item per line, starting with#-------------------------------------------------------------------------------
# a tab.
#-------------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)
#-------------------------------------------------------------------------------
function(multiline oneline)
    # message("** ${oneline}")
    string(REPLACE "  " " "    out ${oneline} )
    # message("** ${out}")
    string(REPLACE "  " " "    out ${out} )
    # message("** ${out}")
    string(REPLACE " "  "\n\t" out ${out} )
    # message("** ${out}")
    set(out "\n\t${out}" PARENT_SCOPE)
    # message("** ${out}")
endfunction(multiline)
