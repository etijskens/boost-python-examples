cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)

#-------------------------------------------------------------------------------
# Find boost include directories, lib Boost.Python, and optionally add
# numpy_boost header-only library.
function(FindBoostPython)
	cmake_parse_arguments(
	    FBP "USE_NUMPY_BOOST" "BOOST_ROOT" "" ${ARGN}
	)
	message("<< FindBoostPython.cmake")
	# message(** ${FBP_USE_NUMPY_BOOST})
	# message(** ${FBP_BOOST_ROOT})
	set( BOOST_ROOT ${FBP_BOOST_ROOT} )
	set( _lib_boost_python  "python${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}" )
	message("<< FIND_PACKAGE(Boost COMPONENTS ${_lib_boost_python} REQUIRED)")
	FIND_PACKAGE(Boost COMPONENTS ${_lib_boost_python} REQUIRED)
	if( VERBOSE )
		message("#  Boost_LIBRARIES    = ${Boost_LIBRARIES}")
		message("#  Boost_LIBRARY_DIRS = ${Boost_LIBRARY_DIRS}")
		message("#  Boost_INCLUDE_DIRS = ${Boost_INCLUDE_DIRS}")
	endif()
	message(">> FIND_PACKAGE(Boost COMPONENTS ${_lib_boost_python} REQUIRED)\n")

	set(BOOST_PYTHON_INCLUDE_DIRS "${PYTHON_INCLUDE_DIRS} ${BOOST_ROOT}"   CACHE INTERNAL "" FORCE)
	set(BOOST_PYTHON_LIBRARIES    "${PYTHON_LIBRARIES} ${Boost_LIBRARIES}" CACHE INTERNAL "" FORCE)

	if( ${FBP_USE_NUMPY_BOOST} )
#	    exec_program( python
#		    ARGS "-c 'import numpy; print(numpy.get_include())'"
#			OUTPUT_VARIABLE NUMPY_INCLUDE_DIRS
#			ERROR_VARIABLE  _e
#	        RETURN_VALUE    NUMPY_NOT_FOUND
#	    )
		execute_process(
	      COMMAND python -c "import numpy; print(numpy.get_include())"
	      RESULT_VARIABLE NUMPY_NOT_FOUND
	      OUTPUT_VARIABLE NUMPY_INCLUDE_DIRS
	      ERROR_VARIABLE  _e
	      OUTPUT_STRIP_TRAILING_WHITESPACE
	      ERROR_STRIP_TRAILING_WHITESPACE
	    )
		if(NUMPY_NOT_FOUND)
	    	message(FATAL_ERROR "NumPy headers not found: ${_e}")
			# FATAL_ERROR since USE_NUMPY_BOOST was REQUIRED
		else()
			if( VERBOSE )
				message("#  NUMPY_INCLUDE_DIRS: ${NUMPY_INCLUDE_DIRS}")
			endif()
			set(NUMPY_BOOST_INCLUDE_DIRS "${NUMPY_INCLUDE_DIRS} ${CMAKE_CURRENT_LIST_DIR}/../et/numpy_boost")
		endif()
		set(BOOST_PYTHON_INCLUDE_DIRS "${BOOST_PYTHON_INCLUDE_DIRS} ${NUMPY_BOOST_INCLUDE_DIRS}" CACHE INTERNAL "" FORCE)
	endif()

	multiline("${BOOST_PYTHON_INCLUDE_DIRS}")
	message(STATUS "BOOST_PYTHON_INCLUDE_DIRS = ${out}")

	multiline("${BOOST_PYTHON_LIBRARIES}")
	message(STATUS "BOOST_PYTHON_LIBRARIES = ${out}")

	string(REPLACE " " ";" BOOST_PYTHON_INCLUDE_DIRS ${BOOST_PYTHON_INCLUDE_DIRS})
	string(REPLACE " " ";" BOOST_PYTHON_LIBRARIES ${BOOST_PYTHON_LIBRARIES})
	set(BOOST_PYTHON_INCLUDE_DIRS "${BOOST_PYTHON_INCLUDE_DIRS}" CACHE INTERNAL "" FORCE)
	set(BOOST_PYTHON_LIBRARIES    "${BOOST_PYTHON_LIBRARIES}"    CACHE INTERNAL "" FORCE)

	message(">> FindBoostPython.cmake")
endfunction()
