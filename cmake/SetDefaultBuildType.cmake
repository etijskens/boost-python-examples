cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)
#-------------------------------------------------------------------------------
# from https://cliutils.gitlab.io/modern-cmake/chapters/features.html
#-------------------------------------------------------------------------------
set(default_build_type "Debug")
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

function(SetBuildType build_type)
  set(CMAKE_BUILD_TYPE "${build_type}")
  message(STATUS "Build type is '${CMAKE_BUILD_TYPE}'")
endfunction()
